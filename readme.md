# Poppler 22.02.0

Built using [mingw-w64-poppler](https://github.com/r-windows/rtools-packages/tree/master/mingw-w64-poppler) from rtools-packages.

This stack only works for R-4.0 and up because poppler requires C++17 now.
