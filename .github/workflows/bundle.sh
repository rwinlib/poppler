#/bin/sh
set -e
PACKAGE=poppler

# Update
pacman -Syy --noconfirm
OUTPUT=$(mktemp -d)

# Download files (-dd skips dependencies)
pkgs=$(echo mingw-w64-{i686,x86_64,ucrt-x86_64}-${PACKAGE})
deps=$(pacman -Si $pkgs | grep 'Depends On' | grep -o 'mingw-w64-[_.a-z0-9-]*')
URLS=$(pacman -Sp $pkgs $deps --cache=$OUTPUT)
VERSION=$(pacman -Si mingw-w64-x86_64-${PACKAGE} | awk '/^Version/{print $3}')

# Set version for next step
echo "::set-output name=VERSION::${VERSION}"
echo "::set-output name=PACKAGE::${PACKAGE}"
echo "Bundling $PACKAGE-$VERSION"
echo "# $PACKAGE $VERSION" > README.md
echo "" >> README.md

for URL in $URLS; do
  curl -OLs $URL
  FILE=$(basename $URL)
  echo "Extracting: $URL"
  echo " - $FILE" >> README.md
  tar xf $FILE -C ${OUTPUT}
  rm -f $FILE
done

# Remove stuff we don't need
rm -Rf ${OUTPUT}/*/lib/{gettext,pkgconfig}
#rm -fv ${OUTPUT}/*/lib/lib{asprintf,charset,iconv,intl,gettext*,gnurx,hogweed,ssl,tre,systre}.a

# Copy libs
rm -Rf lib lib-8.3.0
mkdir -p lib/x64 lib-8.3.0/{x64,i386}
(cd ${OUTPUT}/ucrt64/lib; cp -fv *.a $ROOT/lib/x64/)
(cd ${OUTPUT}/mingw64/lib; cp -fv *.a $ROOT/lib-8.3.0/x64/)
(cd ${OUTPUT}/mingw32/lib; cp -fv *.a $ROOT/lib-8.3.0/i386/)

# Copy headers for mpkg
rm -Rf include && mkdir -p include
cp -Rf ${OUTPUT}/mingw64/include/poppler include/

# Cleanup temporary dir
rm -Rf ${OUTPUT}/*
